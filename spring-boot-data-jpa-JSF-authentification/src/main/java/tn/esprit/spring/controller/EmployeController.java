package tn.esprit.spring.controller;



import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import org.ocpsoft.rewrite.annotation.Join;
import org.ocpsoft.rewrite.el.ELBeanName;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;


import tn.esprit.spring.entity.Employe;
import tn.esprit.spring.entity.Role;
import tn.esprit.spring.service.IEmployeService;



@Scope(value = "session")
@Component(value = "employeController")
@ELBeanName(value = "employeController")
@Join(path = "/", to = "/login.jsf")


public class EmployeController implements IEmployeController {
    
	@Autowired
	IEmployeService iEmployeService;
	private String login; private String password; private Employe employe;
	private Boolean loggedIn;
	
	public String dologin() {
	
		String navigateTo = "null";
		Employe employe=iEmployeService.getEmployeByEmailAndPassword(login, password);
		if (employe != null && employe.getRole() == Role.ADMINISTRATEUR) {
			navigateTo = "/welcome.xhtml?faces-redirect=true";
			loggedIn = true;
			}
			else
			{
			FacesMessage facesMessage =
	                new FacesMessage("Login Failed: please check your username/password and try again.");
	            FacesContext.getCurrentInstance().addMessage("form:btn",facesMessage);
			}
		return navigateTo;	
	}
	
	public IEmployeService getiEmployeService() {
		return iEmployeService;
	}

	public void setiEmployeService(IEmployeService iEmployeService) {
		this.iEmployeService = iEmployeService;
	}

	public String doLogout()
	{FacesContext.getCurrentInstance().getExternalContext().invalidateSession();
	return "/login.xhtml?faces-redirect=true";
	}
	

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Employe getEmploye() {
		return employe;
	}

	public void setEmploye(Employe employe) {
		this.employe = employe;
	}

	public Boolean getLoggedIn() {
		return loggedIn;
	}

	public void setLoggedIn(Boolean loggedIn) {
		this.loggedIn = loggedIn;
	}
	
    
}